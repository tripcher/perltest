#!/usr/bin/perl

use strict;
use warnings;

use PerlTest::Schema;

my $schema = PerlTest::Schema->connect('dbi:Pg:dbname=perl_test', 'perl_test', 'perl_test');

my @users = $schema->resultset('User')->all;

foreach my $user (@users) {
    $user->password('password');
    $user->update;
}