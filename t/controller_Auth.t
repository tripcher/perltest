use strict;
use warnings;
use Test::More;


use Catalyst::Test 'PerlTest';
use PerlTest::Controller::Auth;

ok( request('/auth')->is_success, 'Request should succeed' );
done_testing();
