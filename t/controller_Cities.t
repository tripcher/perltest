use strict;
use warnings;
use Test::More;


use Catalyst::Test 'PerlTest';
use PerlTest::Controller::Cities;

ok( request('/cities')->is_success, 'Request should succeed' );
done_testing();
