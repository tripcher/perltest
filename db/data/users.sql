INSERT INTO users (username, password, email_address, first_name, last_name, active)
    VALUES ('test01', 'password', 't01@na.com', 'Joe',  'Blow', 1);
INSERT INTO users (username, password, email_address, first_name, last_name, active)
    VALUES ('test02', 'password', 't02@na.com', 'Jane', 'Doe',  1);
INSERT INTO users (username, password, email_address, first_name, last_name, active)
    VALUES ('test03', 'password', 't03@na.com', 'No', 'Active',  0);
INSERT INTO role (role) VALUES ('user');
INSERT INTO users_role VALUES (1, 1);
INSERT INTO users_role VALUES (2, 1);
INSERT INTO users_role VALUES (3, 1);

