INSERT INTO country (title, continent_id) VALUES ('Алжир', 1);
INSERT INTO country (title, continent_id) VALUES ('Бенин', 1);
INSERT INTO country (title, continent_id) VALUES ('Габон', 1);

INSERT INTO country (title, continent_id) VALUES ('Афганистан', 2);
INSERT INTO country (title, continent_id) VALUES ('Бутан', 2);
INSERT INTO country (title, continent_id) VALUES ('Вьетнам', 2);

INSERT INTO country (title, continent_id) VALUES ('Канада', 3);
INSERT INTO country (title, continent_id) VALUES ('США', 3);
INSERT INTO country (title, continent_id) VALUES ('Гренландия', 3);

INSERT INTO country (title, continent_id) VALUES ('Австрия', 4);
INSERT INTO country (title, continent_id) VALUES ('Албания', 4);
INSERT INTO country (title, continent_id) VALUES ('Белоруссия', 4);
