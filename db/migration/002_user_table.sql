CREATE TABLE users (
    id            SERIAL PRIMARY KEY,
    username      VARCHAR(100),
    password      VARCHAR(100),
    email_address VARCHAR(100),
    first_name    VARCHAR(100),
    last_name     VARCHAR(100),
    active        INTEGER
);

CREATE TABLE role (
    id   SERIAL PRIMARY KEY,
    role VARCHAR(100)
);

CREATE TABLE users_role (
    user_id INTEGER REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE,
    role_id INTEGER REFERENCES role(id) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (user_id, role_id)
);