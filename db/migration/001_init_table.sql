--
-- Drops just in case you are reloading
---
DROP TABLE IF EXISTS city CASCADE;
DROP TABLE IF EXISTS country CASCADE;
DROP TABLE IF EXISTS continent CASCADE;

CREATE TABLE continent (
    id SERIAL PRIMARY KEY,
    title TEXT NOT NULL DEFAULT ''
);

CREATE TABLE country (
    id SERIAL PRIMARY KEY,
    title TEXT NOT NULL DEFAULT '',
    continent_id INTEGER,
    FOREIGN KEY (continent_id) REFERENCES continent (id) ON DELETE CASCADE

);

CREATE TABLE city (
    id SERIAL PRIMARY KEY,
    title TEXT NOT NULL DEFAULT '',
    countPopulation INT NOT NULL DEFAULT 0,
    country_id INTEGER,
    FOREIGN KEY (country_id) REFERENCES country (id) ON DELETE CASCADE
);