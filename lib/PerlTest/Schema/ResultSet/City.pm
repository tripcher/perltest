package PerlTest::Schema::ResultSet::City;

use strict;
use warnings;
use base 'DBIx::Class::ResultSet';

=head2 search_by_continent


=cut

sub search_by_continent {
    my ($self, $continent_id) = @_;

    return $self->search({'country.continent_id' => $continent_id}, {'join' => 'country'});
}

sub search_group_by_continents {
    my ( $self, @continents ) = @_;

    my @groupByContinent = ();

    foreach my $continent (@continents) {

        my @citiesContinent = ();

        foreach my $city ($self->search({}, { order_by => 'country_id' })) {
            if ($city->country->continent->id == $continent->id) {
                push @citiesContinent, $city
            }
        }

        push @groupByContinent, {
            'title'  => $continent->title,
            'cities' => [@citiesContinent]
        }
    }

    return @groupByContinent;
}
1;