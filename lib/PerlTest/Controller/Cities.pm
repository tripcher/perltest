package PerlTest::Controller::Cities;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

PerlTest::Controller::Cities - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->stash(
        groupByContinent => [$c->model('DbModel::City')->search_group_by_continents($c->model('DbModel::Continent')->all)]
    );

    $c->stash(template => 'cities/index.tt2');
}



=encoding utf8

=head1 AUTHOR

Станислав Шкитин

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
