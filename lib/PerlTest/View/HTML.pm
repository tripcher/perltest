package PerlTest::View::HTML;
use Moose;
use namespace::autoclean;

extends 'Catalyst::View::TT';

__PACKAGE__->config(
    TEMPLATE_EXTENSION => '.tt2',
    render_die => 1,
    ENCODING     => 'utf-8',
    WRAPPER => 'wrapper.tt2'
);

=head1 NAME

PerlTest::View::HTML - TT View for PerlTest

=head1 DESCRIPTION

TT View for PerlTest.

=head1 SEE ALSO

L<PerlTest>

=head1 AUTHOR

Станислав Шкитин

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
